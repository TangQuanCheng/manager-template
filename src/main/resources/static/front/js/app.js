//判断鼠标滚轮滚动方向
if (window.addEventListener)//FF,火狐浏览器会识别该方法
    window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;//W3C
//统一处理滚轮滚动事件
function wheel(event) {
    var positionY = document.documentElement.scrollTop;
    if (positionY > 200) {
        $(".back-to-top").fadeIn(200);
    } else {
        $(".back-to-top").fadeOut(100);
    }
    // dealIntroduce();
}

$(".back-to-top").click(function () {
    scrollToptimer = setInterval(function () {
        var top = document.body.scrollTop || document.documentElement.scrollTop;
        var speed = top / 4;
        if (document.body.scrollTop != 0) {
            document.body.scrollTop -= speed;
        } else {
            document.documentElement.scrollTop -= speed;
        }
        if (top == 0) {
            clearInterval(scrollToptimer);
            $(".back-to-top").hide()
        }
    }, 30);
});

$(".right-nav i").mouseover(function () {
    $(this).prev("span").css({display: 'inline-block'})
})
$(".right-nav i").mouseout(function () {
    $(this).prev("span").css({display: 'none'})
})

var show = 0;

function dealIntroduce() {
    var top = document.body.scrollTop || document.documentElement.scrollTop;
    if (top > 600) {
        $(".fixed-introduce").show();
        $(".closed-introduce").show();
        var left = $(".fixed-introduce").css("left");
        if (show === 0) {
            $(".fixed-introduce").animate({left: 0}, 700);
            $(".closed-introduce").animate({left: '-165px'}, 300);
            show = 1
        }
    } else {
        $(".fixed-introduce").hide();
        $(".closed-introduce").hide();
        var left = $(".fixed-introduce").css("left");
        if (show === 1) {
            $(".fixed-introduce").animate({left: '-100%'}, 700);
            $(".closed-introduce").animate({left: 0}, 300);
            show = 0;
        }
    }
}

$(".online-service").click(function () {
    $("#udesk_btn>a").click();
});
$(document).ready(function () {

// uDesk客服
    (function (a, h, c, b, f, g) {
        a["UdeskApiObject"] = f;
        a[f] = a[f] || function () {
            (a[f].d = a[f].d || []).push(arguments)
        };
        g = h.createElement(c);
        g.async = 1;
        g.charset = "utf-8";
        g.src = b;
        c = h.getElementsByTagName(c)[0];
        c.parentNode.insertBefore(g, c)
    })(window, document, "script", "http://assets-cli.udesk.cn/im_client/js/udeskApi.js", "ud");
    ud({
        "code": "1a3hb1k5",
        "link": "http://decent.udesk.cn/im_client/?web_plugin_id=50365",
        "isInvite": true,
        "mode": "inner",
        "color": "#00572c",
        "pos_flag": "srb",
        "onlineText": "联系客服，在线咨询",
        "offlineText": "客服下班，请留言",
        "mobile": {
            "mode": "inner",
            "targetSelector": ".online-service",
            "color": "#00572c",
            "pos_flag": "crb",
            "onlineText": "联系客服，在线咨询",
            "offlineText": "客服下班，请留言"
        }
    });
    (function () {
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        } else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
})

