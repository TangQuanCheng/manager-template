package cn.decentchina.manager.system.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author 文件上传和下载
 */
@Slf4j
public class FileUtil {

    /**
     * 允许上传的文件后缀
     */
    public static String[] ALLOW_FILE_SUFFIEX = {"jpg", "JPG", "PNG", "png"};

    /**
     * 最大上传
     */
    public static long MAXIMUM_FILE_SIZE = 52428800L;

    /**
     * 上传文件到本地
     *
     * @param file     文件
     * @param fileName 文件名
     * @param path     上传本地文件路径
     * @return 本地文件
     * @throws IOException 文件创建/转换异常
     */
    public static File uploadFile(MultipartFile file, String fileName, String path) throws IOException {
        File targetFile = new File(path, fileName + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        if (!targetFile.exists()) {
            targetFile.createNewFile();
        }
        file.transferTo(targetFile);
        return targetFile;
    }

    /**
     * 检查文件大小
     *
     * @param file
     * @return
     */
    public static boolean checkSize(MultipartFile file) {

        return MAXIMUM_FILE_SIZE >= file.getSize();
    }

    /**
     * 检查文件格式是否被允许
     *
     * @param fileName
     * @return
     */
    public static boolean checkSuffix(String fileName) {
        if (StringUtils.isBlank(fileName)) {
            return false;
        }
        String suffix = StringUtils.split(fileName, ".")[1];
        return Arrays.asList(ALLOW_FILE_SUFFIEX).contains(suffix);

    }

    /**
     * 获取后缀
     *
     * @param fileName
     * @return
     */
    public static String getSuffix(String fileName) {
        return "." + StringUtils.split(fileName, ".")[1];
    }

    /**
     * 导出表格，关闭流
     *
     * @param out
     * @param workbook
     * @throws IOException
     */
    public static void writeAndCloseBook(ServletOutputStream out, HSSFWorkbook workbook) throws IOException {
        try {
            workbook.write(out);
            out.flush();
            out.close();
        } catch (Exception e) {
            log.error("导出失败", e);
        } finally {
            out.flush();
            out.close();
        }
    }
}
