package cn.decentchina.manager.front.dao;

import cn.decentchina.manager.front.vo.FrontOfficialNewsVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 唐全成
 * @date 2018-09-27
 */
@Mapper
@Repository
public interface NewsDao {


    /**
     * 信息分页查询
     *
     * @return
     */
    @Select("select id," +
            " title," +
            " description," +
            " content," +
            " gmt_create as createTime," +
            " face_url as faceUrl," +
            " creator " +
            " from tbl_official_news where official_type='TQ_OFFICIAL' " +
            " order by gmt_create desc " )
    List<FrontOfficialNewsVO> queryList();
    /**
     * 信息分页查询
     * @param limit
     *
     * @return
     */
    @Select("select id," +
            " title," +
            " description," +
            " content," +
            " gmt_create as createTime," +
            " face_url as faceUrl," +
            " creator " +
            " from tbl_official_news where official_type='TQ_OFFICIAL' " +
            " order by gmt_create desc limit 0,#{limit}" )
    List<FrontOfficialNewsVO> queryLastNews(@Param("limit") Integer limit);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Select("select id,title,description,content ,face_url as faceUrl, gmt_create as createTime from tbl_official_news where id=#{id} and official_type='TQ_OFFICIAL'")
    FrontOfficialNewsVO queryById(Integer id);
}
