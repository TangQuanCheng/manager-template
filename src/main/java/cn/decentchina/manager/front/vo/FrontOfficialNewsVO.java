package cn.decentchina.manager.front.vo;

import cn.decentchina.manager.news.entity.OfficialNews;
import lombok.Data;

/**
 * @author 唐全成
 * @date 2018-09-27
 */
@Data
public class FrontOfficialNewsVO extends OfficialNews {
    /**
     * 是否最新
     */
    private Boolean recentNews;
}
