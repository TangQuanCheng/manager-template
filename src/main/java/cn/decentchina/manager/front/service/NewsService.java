package cn.decentchina.manager.front.service;


import cn.decentchina.manager.front.vo.FrontOfficialNewsVO;
import cn.decentchina.manager.system.vo.Page;

import java.util.List;

/**
 * @author 唐全成
 * @date 2018-09-27
 */
public interface NewsService {
    /**
     * 列表查询
     *
     * @param page
     * @return
     */
    Page<FrontOfficialNewsVO> queryList(Page page);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    FrontOfficialNewsVO queryById(Integer id);

    /**
     * 查询最近3条新闻
     * @param limit
     * @return
     */
    List<FrontOfficialNewsVO> queryLastNews(Integer limit);
}
