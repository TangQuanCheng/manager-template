package cn.decentchina.manager.front.service.impl;

import cn.decentchina.manager.front.dao.NewsDao;
import cn.decentchina.manager.front.service.NewsService;
import cn.decentchina.manager.front.vo.FrontOfficialNewsVO;
import cn.decentchina.manager.system.vo.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 唐全成
 * @date 2018-09-27
 */
@Service
public class NewsServiceImpl implements NewsService {

    /**
     * 判断最新新闻标准（一周内）
     */
    private static final Long RECENT_NEWS_TIME = 1000 * 60 * 60 * 24 * 7L;

    @Autowired
    private NewsDao newsDao;

    @Override
    public Page<FrontOfficialNewsVO> queryList(Page page) {
        PageHelper.startPage(page.getPageNumber(), page.getPageSize());
        List<FrontOfficialNewsVO> newsVOS = newsDao.queryList();
        Long now = System.currentTimeMillis();
        newsVOS.forEach(news -> {
            if (now - news.getCreateTime().getTime() <= RECENT_NEWS_TIME) {
                news.setRecentNews(true);
            } else {
                news.setRecentNews(false);
            }
        });
        return new Page<>(newsVOS);
    }

    @Override
    public FrontOfficialNewsVO queryById(Integer id) {
        return newsDao.queryById(id);
    }

    @Override
    public List<FrontOfficialNewsVO> queryLastNews(Integer limit) {
        return newsDao.queryLastNews(limit);
    }
}
