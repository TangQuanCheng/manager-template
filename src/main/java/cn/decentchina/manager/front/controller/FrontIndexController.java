package cn.decentchina.manager.front.controller;
import cn.decentchina.manager.front.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @author 唐全成
 * @date 2018-09-27
 */
@Slf4j
@RestController
@RequestMapping("front")
public class FrontIndexController {


    @Autowired
    private NewsService newsService;
    /**
     * 设备头
     */
    private static final String[] DEVICE_HEAD = {"Windows", "windows", "compatible","Macintosh"};

    /**
     * 首页
     *
     * @return
     */
    @RequestMapping(path = {"", "index.html", "index"})
    public ModelAndView index(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        log.info("设备头：" + header);
        if (StringUtils.containsAny(header, DEVICE_HEAD)) {
            ModelAndView modelAndView = new ModelAndView("front/index");
            modelAndView.addObject("newsList", newsService.queryLastNews(3));
            return modelAndView;
        } else {
            return new ModelAndView("mobile/index");
        }
    }

    /**
     * 来淘券
     *
     * @return
     */
    @RequestMapping(path = {"consume", "consume.html", "consume"})
    public ModelAndView consume() {
        return new ModelAndView("front/consume");
    }

    /**
     * 关于我们
     *
     * @return
     */
    @RequestMapping(path = {"aboutUs", "aboutUs.html", "aboutUs"})
    public ModelAndView aboutUs() {
        ModelAndView modelAndView = new ModelAndView("front/aboutUs");
        modelAndView.addObject("newsList", newsService.queryLastNews(3));
        return modelAndView;
    }

    /**
     * 根据key读取图片
     *
     * @param key
     * @return
     */
    @RequestMapping("/pic")
    public void readPic(String key, HttpServletResponse response) {
        if (!StringUtils.isBlank(key)) {
//            String imageUrl = ossConfig.getPicUrl(key, DateUtils.addYears(new Date(), 10));
//            HttpClientUtil.postStream(imageUrl, 10000, 10000, response);
        }
    }
}
