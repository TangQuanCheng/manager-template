package cn.decentchina.manager.front.controller;
import cn.decentchina.manager.front.service.NewsService;
import cn.decentchina.manager.system.vo.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 唐全成
 * @date 2018-09-27
 */
@RestController
@RequestMapping("front/news")
public class FrontNewsController {

    @Autowired
    private NewsService newsService;

    /**
     * 跳转页面
     *
     * @return
     */
    @RequestMapping("")
    public ModelAndView news(Page page) {
        if(page.getPageNumber()==0){
            page.setPageNumber(1);
        }
        page.setPageSize(5);
        ModelAndView modelAndView = new ModelAndView("front/news");
        modelAndView.addObject("newsList", newsService.queryList(page));
        modelAndView.addObject("recentNews", newsService.queryLastNews(5));
        return modelAndView;
    }

    /**
     * 新闻详情页面
     *
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public ModelAndView newsDetail(@PathVariable Integer id) {
        Page page=new Page();
        page.setPageNumber(1);
        ModelAndView modelAndView = new ModelAndView("front/newsDetail");
        modelAndView.addObject("news", newsService.queryById(id));
        modelAndView.addObject("recentNews", newsService.queryLastNews(5));
        return modelAndView;
    }
}
