package cn.decentchina.manager.news.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 官网类型
 *
 * @author 陈豆豆
 * @date 2018-01-07
 */
public enum OfficialTypeEnum {
    /**
     * 淘券官网
     */
    TQ_OFFICIAL("365淘券"),
    /**
     * 收券官网
     */
    SQ_OFFICIAL("收券吧");

    private final String str;

    OfficialTypeEnum(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public static Map<String, String> getStatusMap() {
        Map<String, String> map = new HashMap<>();
        for (OfficialTypeEnum statusEnum : OfficialTypeEnum.values()) {
            map.put(statusEnum.name(), statusEnum.getStr());
        }
        return map;
    }
}
