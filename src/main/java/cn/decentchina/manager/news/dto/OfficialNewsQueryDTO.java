package cn.decentchina.manager.news.dto;

import cn.decentchina.manager.news.enums.OfficialTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author 唐全成
 * @date 2018-09-25
 */
@Data
public class OfficialNewsQueryDTO {
    /**
     * 查询开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date queryStartTime;
    /**
     * 查询结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date queryEndTime;

    private OfficialTypeEnum officialType;
}
