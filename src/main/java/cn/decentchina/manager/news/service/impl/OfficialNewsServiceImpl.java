package cn.decentchina.manager.news.service.impl;

import cn.decentchina.manager.common.dto.SimpleMessage;
import cn.decentchina.manager.common.enums.ErrorCodeEnum;
import cn.decentchina.manager.common.util.UploadUtil;
import cn.decentchina.manager.config.CommonConfig;
import cn.decentchina.manager.config.Constant;
import cn.decentchina.manager.news.dao.OfficialNewsDao;
import cn.decentchina.manager.news.dto.OfficialNewsQueryDTO;
import cn.decentchina.manager.news.entity.OfficialNews;
import cn.decentchina.manager.news.service.OfficialNewsService;
import cn.decentchina.manager.news.vo.OfficialNewsVO;
import cn.decentchina.manager.system.service.AdminService;
import cn.decentchina.manager.system.util.FileUtil;
import cn.decentchina.manager.system.vo.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author 唐全成
 * @date 2018-09-25
 */
@Slf4j
@Service
public class OfficialNewsServiceImpl implements OfficialNewsService {

    @Autowired
    private OfficialNewsDao officialNewsDao;
    @Autowired
    private AdminService adminService;
    @Resource
    private CommonConfig commonConfig;
//    @Autowired
//    private OSSConfig ossConfig;

    @Override
    public Page<OfficialNewsVO> queryList(Page page, OfficialNewsQueryDTO dto) {
        PageHelper.startPage(page.getPageNumber(), page.getPageSize());
        List<OfficialNewsVO> newsVOS = officialNewsDao.queryList(dto);
        return new Page<>(newsVOS);
    }

    @Override
    public SimpleMessage insertNews(OfficialNews news, MultipartFile newsFace) throws Exception {
        //检查新闻封面图片
        if (newsFace == null) {
            return new SimpleMessage(ErrorCodeEnum.INVALID_PARAMS, "请上传新闻封面图片");
        }
        String fileName = newsFace.getOriginalFilename();
        if (StringUtils.isBlank(fileName)) {
            return new SimpleMessage(ErrorCodeEnum.INVALID_PARAMS, "请上传新闻封面图片");
        }
        if (!FileUtil.checkSuffix(fileName)) {
            return new SimpleMessage(ErrorCodeEnum.INVALID_PARAMS, "上传格式不符");
        }
        if (!FileUtil.checkSize(newsFace)) {
            return new SimpleMessage(ErrorCodeEnum.INVALID_PARAMS, "上传大小不符");
        }
        //获取地址
        String path = this.getClass().getResource("/").getPath();
        String result = UploadUtil.uploadFile(newsFace, path + commonConfig.getUploadPath());
        news.setFaceUrl(StringUtils.replace(result, path + "static", ""));
        if (StringUtils.equals(Constant.FAIL, result)) {
            return new SimpleMessage(ErrorCodeEnum.ERROR, "上传失败");
        }
        try {
            if (officialNewsDao.insert(news) < 1) {
//                ossConfig.getOssClient().deleteObject(ossConfig.getBucketName(), picKey);
                return new SimpleMessage(ErrorCodeEnum.ERROR);
            }
        } catch (Exception e) {
            log.error("新增新闻失败", e);
//            ossConfig.getOssClient().deleteObject(ossConfig.getBucketName(), picKey);
        }
        return new SimpleMessage(ErrorCodeEnum.OK);
    }

    @Override
    public SimpleMessage updateNews(OfficialNews news, MultipartFile file) throws IOException {
        String oldFace = officialNewsDao.queryById(news.getId()).getFaceUrl();
        if (file != null && StringUtils.isNotBlank(file.getOriginalFilename())) {
            String fileName = file.getOriginalFilename();
            if (!FileUtil.checkSuffix(fileName)) {
                return new SimpleMessage(ErrorCodeEnum.INVALID_PARAMS, "上传格式不符");
            }
            if (!FileUtil.checkSize(file)) {
                return new SimpleMessage(ErrorCodeEnum.INVALID_PARAMS, "上传大小不符");
            }
            //获取地址
            String path = this.getClass().getResource("/").getPath();
            String result = UploadUtil.uploadFile(file, path + commonConfig.getUploadPath());
            news.setFaceUrl(StringUtils.replace(result, path + "static", ""));
        }
        if (officialNewsDao.update(news) < 1) {
            return new SimpleMessage(ErrorCodeEnum.ERROR);
        }
        if(StringUtils.isNotBlank(oldFace)){
            // TODO: 2020/12/24 删除原封面
//            ossConfig.getOssClient().deleteObject(ossConfig.getBucketName(), oldFace);
        }
        return new SimpleMessage(ErrorCodeEnum.OK);
    }

    @Override
    public SimpleMessage deleteNews(Integer id) {
        OfficialNews officialNews = officialNewsDao.queryById(id);
        if (officialNews == null) {
            return new SimpleMessage(ErrorCodeEnum.ERROR);
        }
        if (officialNewsDao.delete(id) < 1) {
            return new SimpleMessage(ErrorCodeEnum.ERROR);
        }
//        ossConfig.getOssClient().deleteObject(ossConfig.getBucketName(), officialNews.getFaceUrl());
        return new SimpleMessage(ErrorCodeEnum.OK);
    }
}
