package cn.decentchina.manager.news.service;

import cn.decentchina.manager.common.dto.SimpleMessage;
import cn.decentchina.manager.news.dto.OfficialNewsQueryDTO;
import cn.decentchina.manager.news.entity.OfficialNews;
import cn.decentchina.manager.news.vo.OfficialNewsVO;
import cn.decentchina.manager.system.vo.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author 唐全成
 * @date 2018-09-25
 */
public interface OfficialNewsService {

    /**
     * 列表查询
     *
     * @param page
     * @param dto
     * @return
     */
    Page<OfficialNewsVO> queryList(Page page, OfficialNewsQueryDTO dto);

    /**
     * 新增新闻
     *
     * @param news
     * @param newsFace
     * @return
     * @throws Exception
     */
    SimpleMessage insertNews(OfficialNews news, MultipartFile newsFace) throws Exception;

    /**
     * 修改新闻
     *
     * @param news
     * @param file
     * @return
     * @throws IOException
     */
    SimpleMessage updateNews(OfficialNews news, MultipartFile file) throws IOException;

    /**
     * 删除新闻
     *
     * @param id
     * @return
     */
    SimpleMessage deleteNews(Integer id);
}
