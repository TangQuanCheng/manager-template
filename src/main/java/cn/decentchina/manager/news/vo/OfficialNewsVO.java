package cn.decentchina.manager.news.vo;

import cn.decentchina.manager.news.entity.OfficialNews;
import lombok.Data;

/**
 * @author 唐全成
 * @date 2018-09-25
 */
@Data
public class OfficialNewsVO extends OfficialNews {
}
