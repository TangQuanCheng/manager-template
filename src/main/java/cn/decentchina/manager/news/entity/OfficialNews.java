package cn.decentchina.manager.news.entity;

import cn.decentchina.manager.news.enums.OfficialTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 唐全成
 * @date 2018-09-20
 */
@Data
public class OfficialNews implements Serializable {

    private static final long serialVersionUID = -3464806443196524660L;
    /**
     * id
     */
    private Integer id;
    /**
     * 标题
     */
    private String title;
    /**
     * 简介
     */
    private String description;
    /**
     * 内容
     */
    private String content;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 图片
     */
    private String faceUrl;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 官网类型
     */
    private OfficialTypeEnum officialType;
}
