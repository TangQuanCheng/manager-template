package cn.decentchina.manager.news.dao.provider;


import cn.decentchina.manager.news.dto.OfficialNewsQueryDTO;

import java.util.HashMap;

/**
 * @author 唐全成
 * @date 2018-09-25
 */
public class OfficialNewsDaoProvider {
    /**
     * 信息分页查询
     *
     * @param map
     * @return
     */
    public String queryNewsList(HashMap<String, Object> map) {
        StringBuffer sql = new StringBuffer(200);
        sql.append("select id," +
                " title," +
                " description," +
                " content," +
                " gmt_create as createTime," +
                " face_url as faceUrl," +
                " official_type as officialType," +
                " creator " +
                " from tbl_official_news " +
                " where 1=1 ");
        queryCondition(map, sql);
        sql.append(" order by gmt_create desc");
        return sql.toString();
    }

    /**
     * 查询条件封装
     *
     * @param map
     * @param sql
     */
    private void queryCondition(HashMap<String, Object> map, StringBuffer sql) {
        OfficialNewsQueryDTO dto = (OfficialNewsQueryDTO) map.get("dto");
        if (dto.getQueryStartTime() != null) {
            sql.append(" and gmt_create >=#{dto.queryStartTime}");
        }
        if (dto.getQueryEndTime() != null) {
            sql.append(" and gmt_create <=#{dto.queryEndTime}");
        }
        if (dto.getOfficialType() != null) {
            sql.append(" and official_type =#{dto.officialType}");
        }
    }
}
