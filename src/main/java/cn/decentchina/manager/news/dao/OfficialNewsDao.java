package cn.decentchina.manager.news.dao;

import cn.decentchina.manager.news.dao.provider.OfficialNewsDaoProvider;
import cn.decentchina.manager.news.dto.OfficialNewsQueryDTO;
import cn.decentchina.manager.news.entity.OfficialNews;
import cn.decentchina.manager.news.vo.OfficialNewsVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 唐全成
 * @date 2018-09-25
 */
@Mapper
@Repository
public interface OfficialNewsDao {

    /**
     * 新增
     *
     * @param officialNews
     * @return
     */
    @Insert("insert into tbl_official_news(" +
            " title," +
            " description," +
            " content," +
            " gmt_create," +
            " face_url," +
            " official_type," +
            " creator) values(" +
            " #{news.title}," +
            " #{news.description}," +
            " #{news.content}," +
            " now()," +
            " #{news.faceUrl}," +
            " #{news.officialType}," +
            " #{news.creator})")
    int insert(@Param("news") OfficialNews officialNews);

    /**
     * 修改
     *
     * @param officialNews
     * @return
     */
    @Update("update tbl_official_news set " +
            " title=#{news.title}," +
            " description=#{news.description}," +
            " content=#{news.content}," +
            " face_url=#{news.faceUrl}," +
            " official_type=#{news.officialType}," +
            " gmt_modified=now() " +
            " where id=#{news.id} ")
    int update(@Param("news") OfficialNews officialNews);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Delete("delete from tbl_official_news where id=#{id}")
    int delete(@Param("id") Integer id);


    /**
     * 信息分页查询
     *
     * @param dto 搜索条件
     * @return
     */
    @SelectProvider(type = OfficialNewsDaoProvider.class, method = "queryNewsList")
    List<OfficialNewsVO> queryList(@Param("dto") OfficialNewsQueryDTO dto);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Select("select id,title,description,content ,face_url as faceUrl from tbl_official_news where id=#{id}")
    OfficialNews queryById(Integer id);
}
