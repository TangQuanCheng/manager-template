package cn.decentchina.manager.news.controller;

import cn.decentchina.manager.common.dto.SimpleMessage;
import cn.decentchina.manager.common.util.UploadUtil;
import cn.decentchina.manager.config.CommonConfig;
import cn.decentchina.manager.news.dto.OfficialNewsQueryDTO;
import cn.decentchina.manager.news.entity.OfficialNews;
import cn.decentchina.manager.news.service.OfficialNewsService;
import cn.decentchina.manager.news.vo.OfficialNewsVO;
import cn.decentchina.manager.system.util.FileUtil;
import cn.decentchina.manager.system.vo.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 唐全成
 * @date 2018-09-25
 */
@Slf4j
@RequestMapping("news")
@RestController
public class OfficialNewsController {

    @Autowired
    private OfficialNewsService officialNewsService;

    @Autowired
    private CommonConfig commonConfig;

    @RequestMapping("")
    public ModelAndView news() {
        return new ModelAndView("official/news");
    }

    /**
     * 数据列表
     *
     * @param page
     * @param dto
     * @return
     */
    @RequestMapping("list")
    public Page<OfficialNewsVO> queryList(Page page, OfficialNewsQueryDTO dto) {
        return officialNewsService.queryList(page, dto);
    }

    /**
     * 新增
     *
     * @param news
     * @return
     */
    @RequestMapping("add")
    public SimpleMessage addNews(OfficialNews news, @RequestParam("addFaceImg") MultipartFile addFaceImg) throws Exception {
        return officialNewsService.insertNews(news,addFaceImg);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public SimpleMessage deleteNews(@PathVariable Integer id) {
        return officialNewsService.deleteNews(id);
    }

    /**
     * 修改
     *
     * @param news
     * @return
     */
    @RequestMapping("update")
    public SimpleMessage updateNews(OfficialNews news,@RequestParam("editFaceImg") MultipartFile editFaceImg) throws IOException {
        return officialNewsService.updateNews(news,editFaceImg);
    }

    @RequestMapping("uploadPic/list")
    public Map<String,Object> uploadPic(MultipartFile imgFile, HttpServletRequest httpServletRequest){
        Map<String,Object> result=new HashMap<>(4);
        String path = this.getClass().getResource("/").getPath();
        if (imgFile == null) {
            result.put("error",1);
            result.put("message","请选择要上传的图片");
            return result;
        }
        File updateFile = null;
        try {
            updateFile = FileUtil.uploadFile(imgFile, imgFile.getName(), path);
            String picKey = "official/" + System.currentTimeMillis() + imgFile.getName() + "pic";
//            ossConfig.getOssClient().putObject(new PutObjectRequest(ossConfig.getBucketName(), picKey, updateFile));
//            result.put("error",0);
//            result.put("url",ossConfig.getPicUrl(picKey, DateUtils.addYears(new Date(), 10)));

            String imgLocation = UploadUtil.uploadFile(imgFile, path + commonConfig.getUploadPath());
            result.put("error",0);
            result.put("url", commonConfig.getDomain()+StringUtils.replace(imgLocation, path + "static", ""));

            return result;
        } catch (IOException e) {
            log.error("新闻图片上传失败", e);
            result.put("error",1);
            result.put("message","新闻图片上传失败");
            return result;
        } finally {
            FileUtils.deleteQuietly(updateFile);
        }
    }

}
